# C++ part

## Report

This report illustrates rapidly how the C++ project has been structured and developed, focusing on some key implementation choices.
The project is a binary search tree implementation for C++ with templates on the key and value type, plus a template on the order relation, which determines the hierarchy of the nodes within the tree.

### Project and Folder Structure

The header file for the binary search tree is called `bst.h` and is found within the `./src` folder.
Two source code files are provided within the `./include` folder:

- `bst.cpp` is an example of use of the binary search tree within C++ and may be used to try the functionalities of the tree;
- `bst_py.cpp` contains the code to expose the binary search tree object, with its public methods, within a Python shared library. The exposure is carried out using Pybind11.

Additionally, a Doxygen configuration file is provided within the `./Doxygen` folder.

### How to compile

Note: to compile the binary search tree as a Python library, please refer to the documentation for the mix part.

To compile the `bst.cpp` source code, just call the `makefile` within the `c++` folder.

Alternatively, for manual compilation, justy type `g++ -std=c++11 src/bst.cpp` into the terminal.

### How to run

After compilation, run the corresponding `a.out`/`a.exe` file found within the `c++` folder from a terminal.

### Implementation report

Please refer to the `bst.h` file for this section.

The class `bst` is the object implementing the binary search tree.
It requires two templates to be specified by the user:

- `K` is the type of the key of each node of the tree;
- `V` is the type of the value of each node of the tree;

Additionally, an order relation may be specified as a third template. This relation specifies the hierarchy wrt which the nodes are ordered within the tree. The default relation is `<`, passed to the template with the binary function class wrapper `std::less`, which itself is templated on the key type `K`.

The `bst` object itself is very light since it only stores two class members:

- a pointer to the head of the tree
- an integer storing the size of the tree (i.e. how many nodes it has)

The tuples (key, value) are instead stored within the `Node` class, internal to `bst`. A `Node` is a class (private within `bst`) which stores information about the key, the value, and the connections that the node has within the tree.
Note that:

- Since the key is `const` (immutable, since its modification may cause violation in the tree hierarchy), we construct the `Node` passing the key by ref; on the other hand, the value is passed by val, thus causing the specified value object to be copied; this favors trees whose template on the value type is related to small objects (e.g. `int`, so I can insert nodes by passing rvalues of value); on the other way, creating a node of, for instance, a large `vector` as value may result inefficient. A different implementation could've accepted the value by ref in order to favor values templated to larger objects.
- The `Node` has ownership over the pointers to its left and right children (they're `unique_ptr`s); for this same reason, the pointer to its father is a naked pointer and not a smart pointer.

Moreover, `Node` uses an enum class called `childhoodType` to store information whether a given `Node` is a left child, a right child, or is the root/head of the tree. This is a feature of great utility for tree traversal, node insertion and deletion.

A `Node` has getters and setters to modify internal members: note that setters for child also call setters for father on the child.

Copy and move constructor are also provided (the former copying only key and value, but not the pointers), while copy and move assignments are `delete`d since we don't wish our nodes' key to be modified thus violating the tree hierarchy.

`Node` also provide methods to navigate the tree using `Node`'s pointers. Those methods are promptly called within the tree's iterator implementation. The reason for providing traversing functions within the `Node` and not solely within the iterators is the following:

- within the tree we need multiple methods to traverse the tree while getting multiple informations (key, value and connections) about the current `Node`;
- an iterator, though, should only provide access to the `Node` related to the dereferenciation operator `*` (in our case, the value type);
- hence, we provide the core functions for traversing in `Node` while limiting the function in the iterators to mere wrappers of those methods in order to avoid code duplication.

A notable traversing method is `navigateTo`, which tells whether the given `Node` exists in the subtree defined by the `Node` the method is called on and, if not, the node that should be its father in case a new node should be inserted. This provides a common logic for inserting/finding a key in the tree top-to-bottom in a recursive fashion and avoids code duplication in numerous parts of the project. A version of `navigateTo` exists as a private method also in the `bst` class which simply calls `navigateTo` on the head.
`navigateTo` uses an `enum class` called `destinationNodeLocation` which tells whether the returned node is the tree to be searched for (value `here`) or is a candidate father of it (and hence the node must be inserted - value `left` or `right`) or, moreover, there's no node in the tree and the "candidate father" is hence the `head` of the tree (value `head`).

Other methods for tree navigation are `smallestOfBigger` (which finds the smallest - wrt the order relation - `Node` between the bigger `Node`s of a given `Node`), `biggestOfSmaller`, and `next`, which travels to the next element of the tree wrt the order hierarchy.


As cited above, `bst` holds yet two public classes within itself, `Iterator` (which inherits from `std::iterator`) and `ConstIterator`, both providing public methods to traverse the tree and access its elements (`++`, `*`). Note that the traversing methods within `Node` are private outside of `bst`, being it a private class.

A `bst` has a default constructor which initializes the head to `nullptr` and the size to 0.
A copy constructor is given that recursively deep-copies the nodes in a top-down fashion, while re-constructing the links bottom-up (`copy_recursive` method).
The move constructor, instead, just releases the ownership of the head of the moved tree, passing it to the new tree, while moving the size integer as well.
The copy and move assignment operators are implemented in the same way as the respective constructors. 


New nodes in the tree may be inserted using the `insert` method, which itself does three things:

1. Locate the candidate father of the new node (via `navigateTo` helper method)
2. In case the node already exists, do nothing and return `false`
3. Insert a new node below it (respecting the tree hierarchy), using `insertBelow` and return `true`

Extraction of those two helper methods have been necessary in order to avoid code and logic duplications within other parts of the project.

A node may be found using two methods:

1. `find`, which accepts a key and returns an `Iterator` to the given node (`end()` in case the node doesn't exist);
2. `operator[]`, which instead returns the value corresponding to the desired key, inserting a new node if the key doesn't exist.

Both methods use the logics dictated by `navigateTo`.

A tree may be `print`ed: `print` traverse the tree from the smallest (wrt the key) to the largest node (the same way a forward `Iterator` would do) while providing information for both the key and the value of every node.

A node may be deleted using the `erase` method, which deletes a `Node` corresponding to a given key. Like `insert`, if the node's not found, `false` is returned; otherwise, the node is erased while reconstructing its subtree searching for a candidate for the new head (the largest node of the nodes smaller nodes of the deleted node within its subtree) on the left side of the subtree; if such left candidate doesn't exist (because the deleted node had no left children), a candidate is searched for in the right subtree (and hance is the smallest of the larger nodes). After the subtree is reconstructed, its head is attached to the deleted node's father and `true` is returned.

A tree may be `balance`d (re-arranged such that its height is minimum) to provide better performances on lookup functions. A `balance`ment is performed flattening the tree in a vector of pointer to `Node`s (with the order relation preserved in the vector's indices), releasing the head of the ownership of the pointer (thus emptying the tree without deleting its old nodes), splitting the vector in subvectors and recursively getting the median of them and re-inserting the corresponding `Node` within the tree.

Finally, the methods `begin` and `end` (provided also in the corresponding variants for `ConstIterator`) re-use traversing methods to allow for `Iterator`s to be accessed in the standard library's way.

### Tree performance
The performances of the `find` function has been measured on both an unbalanced tree and a balanced one.
To time the application we used the `<chrono>` library.
The timings are obtained using the files contained in the `performance` folder.
As we can see from the provided charts, the unbalanced tree scales linearly, and the balanced one scaled logaritmically with the number of nodes 


![](./performance/both.png)

Since the difference in scales is some orders of magnitude to appreciate the logaritmic scaling of the balanced tree a second chart is provided:
![](./performance/timingsBalanced.png)

### What you have to submit

- You have to upload only and all the **source files** you wrote, with a
**Makefile** (or **CMakeLists.txt**) and a **readme.md** file where you describe how to compile, run your code and a short report on what you have done and understood.

- your code should have no memory leaks. You can check running
```
$ valgrind ./a.out ...
```
where the `...` means possible additional command line arguments, if any.

- your code must be compiled with the flags `-Wall -Wextra` and no warnings must appear


## Binary search tree

In this exam you are required to implement a **template** binary search tree (BST). A BST, is a hierarchical (ordered) data structure where each **node** can have at most two children, namely, **left** and **right** child. Each node stores a **pair** of a **key** and the associated **value**. The binary tree is ordered according to the keys. Given a node `N`, all the nodes having keys **smaller** than the key of the node `N` can be found going **left**. While all the nodes with a key **greater** than the key of the node `N` can be reached going **right**.

![](./.aux/binary.png)


Practically speaking, given the binary tree in the picture, if you need to insert a new node with key=5, you start from the root node `8`, you go left since `5<8`, you reach node `3`, then you go right, you land in `6`, you go left reaching node `4`. Node `4` has no right child, so the new node `5` will be the right child of node `4`. If a key is already present in the tree, you can choose if replace the value with the newest one, or leave the things as they are.

From the implementation point of view, you node has two pointers `left` and `right` pointing to the left and right child respectively. The pointers points to `nullptr` if they have no children.

It is useful to add an additional pointer (head, root, whatever you like) pointing to the first node, called **root node**.

### Tree traversal

The tree must be traversed in order, i.e., if I "print" the tree in the picture, I expect to see on the screen the sequence
```
1 3 4 6 7 8 10 13 14
```
node `1` is the first node, and node `14` is the last one.

### Assignments
You have to solve the following tasks in C++11 (C++14 and 17 are welcomed as well).

  - implement a **template** binary search tree
    - it must be templated on the type of the key and the type of the value associated with it.
    - **optional** you can add a third template on the operation used to compare two different keys.
    - implement proper iterators for your tree (i.e., `iterator` and `const_iterator`)
    - the tree must have at least the following public member function
      1. `insert`, used to insert a new pair key-value.
      2. `print`,  used to print `key: value` of each node. Note that the output should be in order with respect to the keys.
      3. `clear()`, clear the content of the tree.
      4. `begin()`, return an `iterator` to the first node (which likely will not be the root node)
      5. `end()`, return a proper `iterator`
      6. `cbegin()`, return a `const_iterator` to the first node
      7. `cend()`, return a proper `const_iterator`
      8. `balance()`, balance the tree.
      9. `find`, find a given key and return an iterator to that node. If the key is not found returns `end()`;
      9. **optional** `erase`, delete the node with the given key.
      10. **optional** implement the `value_type& operator[](const key_type& k)` function int the `const` and `non-const` versions). This functions, should return a reference to the value associated to the key `k`. If the key is not present, a new node with key `k` is allocated having the value `value_type{}`. 

    - implement copy and move semantics for the tree.

- test the performance of the lookup (using the function `find`) before and after the tree is re-balanced. Use proper numbers (and types) of nodes and look-ups. Does lookup behaves as `O(log N)`?
- **optional** document the code with `Doxygen`
- write a short report
- test everything

### Hints
- you can use `std::pair<key_type,value_type>` found in the header `utility`
- use recursive functions
- **Big hint** start coding and using the iterators ASAP.

