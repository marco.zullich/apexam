#ifndef BST_H
#define BST_H

#include <iostream>
#include <functional>
#include <memory>
#include <vector>
#include <iterator>

using namespace std;

//! Enum useful for finding/inserting in the tree
/*! Where is the node we're searching for with respect to another node? */
enum class destinationNodeLocation : int {here, left, right, head};

//!Enum - what kind of child is a given tree node?
enum class childhoodType : char {root = 'H', leftChild = 'L', rightChild = 'R'};

//! Binary Search Tree
/*! Implementation of a binary search tree for C++.
A tree is composed of nodes each holding a left and a right connection to another node.
A node is characterized by a key which univoquely identifies the node in the tree, and a value.
The left and right connection is determined to comply to an order relation:
a node  \f$ N \f$ is left child of a node \f$ F \f$ if \f$ N.key\prec F.key \f$.
The implementation is hence templated on the key type `K`, the value type `V`
and the order relation `C`, which defaults to `std::less<K>`; another order relation may be passed
by specifying a `struct` wrapping such relation.
The binary search tree offers a very efficient routine for searching a given node in the tree.*/
template <typename K, typename V, typename C = less<K>>
class bst{

private:
    
    

    
    class Node;
    
    //! The root (northernmost) entry of the tree.
    unique_ptr<Node> head;
    
    //! Integer counting the no. of nodes in the tree.
    size_t _size;

public:
    //! Default constructor, to create an empty tree
    bst (): head{nullptr}, _size{0} {}
    
    //! Copy constructor
    bst(const bst& other);

    //! Copy assignment
    bst& operator=(const bst& other);


private:
    //! Helper method to copy recursively starting from a given node
    /*! Copies, in a top-down fashion, the given node and all of its children.
        Then, in a bottom-up fashion, rebuilds all of the links up to the given node.*/
    Node* copy_recursive (const Node* local_head) const ;

public:
    //! Move constructor
    /*! Just moves the head from a tree to the recipient using the release method of the smart pointers.
        Then copies the size and resets it to 0 for the moved tree.*/
    bst(bst && other ):head{other.head.release()},_size{other._size}{other._size=0;}

    //! Move assignment 
    /*! Just moves the head from a tree to the recipient using the release method of the smart pointers.
        Then copies the size and resets it to 0 for the moved tree.*/
    inline bst& operator= (bst&& other){
        head=std::move(other.head);
        _size=std::move(other._size);
        other._size=0;
        return *this;
    }

    //! Insert a (key, val) tuple within the tree
    bool insert(const K& key, const V val) noexcept;


    //! Clears the whole content of the tree
    /*! Makes use of the reset method of the unique_ptr which destroys the object pointed to;
        doing so, the destruction is propagated to all of the children, ensuring absence of leaks.*/
    inline void clear(){this->_size=0; this->head.reset();}

    //! Deletes a single node from the tree
    /*! Within the tree, the node is represented by the parameter key, so the corresponding node
        (if existing) is searched for and eliminated while reconstructing the subtree formed by
        the node itself (root of the subtree), its children and the following children.
        The reconstruction is carried out searching for a candidate of the root of the subtree
        from the left hand side of the subtree itself; if no node is present on this side of the subtree,
        the candidate replacement is searched for in the right side.
        Returns true if the node is found (and erased), false if it's not found.*/
    bool erase(const K& key) noexcept;
private:
    
    //utility function for erase, make the substitution and `erase` the old node
    //Node* swapNodes(Node* oldNode, Node* newNode); UNUSED
    
    //! Function which returns the node having the given key.
    /*! If no node is found, returns `nullptr`. Used as helper mainly in `find`.*/
    Node* getNode(const K& key);

    //! Inserts the given node below the given father.
    /*! `destinationNodeLocation` indicates whether the node should be inserted as left or right 
        children of the father.*/
    bool insertBelow(unique_ptr<Node>& nodeToInsert, pair<Node*, destinationNodeLocation> father); //Note: pair
                                                                //passed by val since small
    //! Another version of `insertBelow` to use with `childhoodType`.
    bool insertBelow(unique_ptr<Node>& nodeToInsert, Node* father, childhoodType nodeChType);

    //! Inverts given childhoodType
    /*! `leftChild` becomes `rightChild` and viceversa. If `root`, throws an exception.*/
    childhoodType revertChType(childhoodType c) const;

    //! Get the first node of the tree wrt the templated order relation.
    Node* getSmallest() const;

    //! Flattens the tree in a `vector`
    /*! The vector is ordered wrt the templated order relation.
        No operation is actually performed on the tree, since the pointers to `Node` in the vector
        point to the same memory location of the nodes of the tree and are not smart, hence not causing
        concurrency issues to the other `unique_ptr`s.
        Used to balance the tree*/
    unique_ptr<vector<Node*>> flatten() const;


    //! Wrapper for the recursive function `Node::navigateTo` to navigate the tree starting from its head.
    pair<Node*, destinationNodeLocation> navigateTo(const K& k) ;

    

public:
    
    class Iterator;
    //! Returns an iterator to the first element of the tree (wrt the order relation).
    Iterator begin() noexcept;
    //! Returns an iterator to `nullptr`, being the element beyond the last one (wrt order relation).
    Iterator end() noexcept {return Iterator(nullptr);}

    class ConstIterator;
    ConstIterator begin() const noexcept;
    ConstIterator end() const noexcept{ return ConstIterator{nullptr}; }

    ConstIterator cbegin() const noexcept {return begin();}
    ConstIterator cend() const noexcept { return ConstIterator{nullptr}; }

    //! Returns an iterator to the node having the given `key`
    /*! If not found, returns `end()`*/
    Iterator find(const K& key) noexcept;


    //! Returns the value corresponding to the node having the given `key`.
    /*! If no suitable node exists, it inserts a new one in the tree with the given `key` and the
        default for the templated value type.*/
    V& operator[] (const K& key) noexcept;
    //! Const variant of `operator[]`
    const V& operator[]  (const K& key) const noexcept {return *this[key];}

    //! Prints the tree in the standard output stream.
    /*! The print is performed from `begin()` to `end()` preserving the templated order relation within
        the tree.*/
    void print(bool printStructure = false) noexcept;


    //! Balances the tree in order to keep the height of the tree minimal.
    /*! Balancing is performed by first flattening the tree in a vector, then re-inserting the nodes in a new
        substitute tree. The insertion is performed by recursively splitting the vector wrt its median and 
        inserting the median itself in the substitute tree.*/
    void balance() noexcept;

private:
    //! Recursively insert into the tree the median of the subvector of `flat` identified by begin and end indices.
    /*! `newFather` and `medianChildhoodType` help efficientizing the insertion operation. Used within `balance`*/
    void insertMedian(unique_ptr<vector<Node*>>& flat, int beginIndex, int endIndex, Node* newFather = nullptr,
                                childhoodType medianChildhoodType = childhoodType::root) ;

    //! Recursively release all nodes from the tree bottom-up.
    void releaseAllNodes() noexcept;

};


//! Entity representing a tree entry.
/*! The Node stores info regarding the key and value of the given entry and is linked to the next
    entries (left and right children) within the tree itself.*/
template <typename K, typename V, typename C>
class bst<K,V,C>::Node {
private:
    //! The key of the node.
    /*! `const` is enforced in order not to violate the order relation in case of modification of the key.*/
    const K k;
    //! The value of the node.
    V v;
    //! The left child of the node.
    unique_ptr<Node>  left;
    //! The right child of the node.
    unique_ptr<Node> right;
    //! The father of the node.
    /*! Isn't `unique_ptr` since the ownership is already enforced by the child pointer of the father.*/
    Node* up;
    
    //! Type of child of the node.
    /*! Answers the question "Which kind of child is this node?".*/
    childhoodType c;
    //! Helper method for `next`.
    void evalChildhoodType(Node**) ;
public:

    //TODO: see if can delete
    Node(){}
    //! Standard constructor.
    Node(const K key, V val): k{key}, v{val}, left{nullptr},
                        right{nullptr}, up{nullptr}, c{childhoodType::root} {}
    //! Copy constructor.
    /*! Copies only the content (key, value pair) without preserving the links to other nodes.*/          
    Node(const Node& other);

    //! Move constructor.
    inline Node(Node&& other): k{other.k}, v{other.v}, left{other.left.release()},
                        right{other.right.release()}, up{other.up}, c{other.c} {other.k = K{};
                        other.v = V{}; c = childhoodType::root;};


    //! Copy assignment deleted
    /*! Can't allow modification of the key of current node*/
    Node& operator=(const Node& other) = delete;

    //! Move assignment deleted
    /*! Can't allow modification of the key of current node*/
    Node& operator=(Node&& other) = delete;


    //! Returns pointer to the child of the specified kind.
    /*! If `childhoodType` isn't `leftChild` or `rightChild`, throws an exception.*/
    Node* getChild(childhoodType ch) const;
    
    //! Returns pointer to the left child of the node.
    Node* getLeftChild()  noexcept {return left.get();} 
    //! Returns `const` pointer to the left child of the node.
    const Node* getLeftChild() const noexcept {return left.get();}
    //! Returns pointer to the right child of the node.
    Node* getRightChild()  noexcept {return right.get();}
    //! Returns `const` pointer to the right child of the node.
    const Node* getRightChild() const noexcept {return right.get();}

    //! Return pointer to the father of the node.
    Node* getFather() const noexcept {return up;}

    //! Returns closest node with higher hierarchy wrt order relation
    Node * next() noexcept;
    
    //! Returns pointer to the desired child of the node and releases ownership from current node.
    /*! If `childhoodType` isn't `leftChild` or `rightChild`, throws an exception.*/
    Node* releaseChild(childhoodType ch) ;
    

    //! Returns pointer to the left child of the node and releases ownership from current node.
    Node* releaseLeftChild() noexcept;
    //! Returns pointer to the right child of the node and releases ownership from current node.
    Node* releaseRightChild() noexcept;

    //! Releases current node from tree.
    /*! Call releaseChild on father. Doesn't work on head since it must be relased from the tree.*/
    Node* release() ;

    //! Recursively releases node and its children.
    Node* release_recursive() ;

    //! Returns the key of the node.
    /*! `const` is enforced in order to avoid modifications to the key.*/
    const K& getKey() const noexcept {return k;}

    //! Returns the value of the node.
    V& getVal() noexcept {return v;}
    
    //! Returns the `childhoodType` (which kind of child -left or right- hte node is) of the node.
    childhoodType getChildhoodType() const noexcept{return c;}

    //TODO: see if i can remove & from unique_ptr
    //! Sets the child of the specified type for the current node
    /*! If `childhoodType` isn't `leftChild` or `rightChild`, throws an exception.*/
    void setChild(unique_ptr<Node>& node, childhoodType cht);
    

    //! Sets the left child or the current node
    /*! Also, sets the father for the new child as `this`*/
    inline void setLeftChild(unique_ptr<Node>& child) noexcept {left.swap(child); left->setFather(this, childhoodType::leftChild);}
    //! Sets the left child or the current node
    /*! Also, sets the father for the new child as `this`*/
    inline void setRightChild(unique_ptr<Node>& child) noexcept {right.swap(child); right->setFather(this, childhoodType::rightChild);}
    //! Sets father and `childhoodType` for the node
    inline void setFather(Node* node, const childhoodType chtype = childhoodType::root) noexcept{up = node; c = chtype;}

    //! Updates the value for the node
    void setVal( V val) noexcept {v = val;}
    //void setKey(const K val) {k = val;} - Can't do this outside of c'tors
    //void setChildhoodType(const childhoodType val) {c = val;} - unused and possibly dangerous

    //! Evaluates equality of two nodes by their key
    bool operator==(const Node& other) const {return this->key_equals(other.getKey());}
    //! Evaluates inequality of two nodes by their key
    bool operator!=(const Node& other) const {return !(*this==other);}

    //! Assesses equality between key of node and other key using templated order relation
    bool key_equals(const K& key) noexcept{return !(C()(k, key) || C()(key, k));}

    //! Assesses superiority between key of node and other key using templated order relation
    bool key_superior(const K& key) noexcept{return C()(key, k);}

    //! Searches for the largest nodes between the nodes smaller than the input node.
    /*! Used in `erase` as helper method.*/
    Node* biggestOfSmaller() noexcept;
    //! Searches for the smallest nodes between the nodes larger than the input node.
    /*! Used in `erase` and `next` as helper method.*/
    Node* smallestOfBigger() noexcept;

    //! Search for the node closest to the given key, top-down from the node
    /*! Operates a recursive search in the subtree, whose head is `this` node, of the node
        containing the key. If such node doesn't exist, the node (having the appropriate
        left or right child spot free) closest to the key is returned.
        The enum `destinationNodeLocation` is needed to give further information about the node
        returned:
        - `here` tells that the node is the one with the given key
        - `leftChild` tells that the node with the given key isn't in the tree, and should be 
            found as left child of the returned node
        - `rightChild` - as above, but with right child instead of left child
        - `head` tells that the tree is empty, and the given key should be found as head of the tree
    */
    pair<Node*, destinationNodeLocation> navigateTo (const K& key) ;

};

//! The iterator (non-`const`) to traverse the tree.
/*! Is a forward iterator, hence can be used only to move wrt the templated order relation (using the operator `++`)
    and not backwards.*/
template <typename K, typename V, typename C>
class bst<K,V,C>::Iterator : public std::iterator<std::forward_iterator_tag, V>{
    using Node = bst<K,V,C>::Node;
    Node* actual;

public:
    Iterator(Node* node) : actual{node} {}
    V& operator*() const {return actual->getVal();}
    //vedere se fare actual==other.actual o *actual==*(other.actual)
    bool operator==(const Iterator& other) const {return actual==other.actual;}
    bool operator!=(const Iterator& other) const {return !(*this==other);}



public:
    Iterator& operator++(){
        actual=actual->next();
        return *this;
    }
};

//! Const version of Iterator
template <typename K, typename V, typename C>
class bst<K,V,C>::ConstIterator : public bst<K,V,C>::Iterator {
    using parent = bst<K,V,C>::Iterator;

public:
    using parent::Iterator;
    const V& operator*() const { return parent::operator*(); }
};


//>>>>>>>>>>>>>>>>>>>>>>METHODS FOR BST






//copy constructor for bst
template <typename K, typename V, typename C>
bst<K,V,C>::bst(const bst<K,V,C>& other): _size{other._size}{
    
    this->head = unique_ptr<bst::Node>(new bst::Node(*other.head.get()));
    Node* temp = copy_recursive(other.head.get());
    this->head =unique_ptr<bst::Node>(temp);
    

}
//! Copy assignment
template <typename K, typename V, typename C>
bst<K,V,C>& bst<K,V,C>::operator=(const bst<K,V,C>& other){

    this->head = unique_ptr<bst::Node>(new bst::Node(*other.head.get()));
    Node* temp = copy_recursive(other.head.get());
    this->head =unique_ptr<bst::Node>(temp);
    return *this;
}

//copy recursive function bst
template <typename K, typename V, typename C>
typename bst<K,V,C>::Node* bst<K,V,C>::copy_recursive(const  bst<K,V,C>::Node* local_head //!the head of the subtree to which apply the method
                                                        ) const {
    //call copy constructor for the new node
    bst::Node* loc_Node {new bst::Node(*local_head)};

    //if old node has child, apply the method to them, then set new nodes as children of new Node
    if (local_head->getLeftChild()!=nullptr) {
        const bst::Node* left_ptr = local_head->getLeftChild();
        unique_ptr<Node> left_node {unique_ptr<Node>(copy_recursive(left_ptr))};
        loc_Node->setLeftChild(left_node);
    }
    if (local_head->getRightChild()!=nullptr) {
        const bst::Node* right_ptr = local_head->getRightChild();
        unique_ptr<Node> right_node {unique_ptr<Node>(copy_recursive(right_ptr))};
        loc_Node->setRightChild(right_node);
    }

    return loc_Node;  
    
}



template <typename K, typename V, typename C>
pair<typename bst<K,V,C>::Node*, destinationNodeLocation> bst<K,V,C>::navigateTo(const K& key) {
    //if tree empty, give info that recipient key should be head node
    if(head.get()==nullptr){
        return make_pair(nullptr, destinationNodeLocation::head);
    }
    //otherwise, apply Node::navigateTo
    return head->navigateTo(key);
}

template <typename K, typename V, typename C>
typename bst<K,V,C>::Node* bst<K,V,C>::getNode(const K& key){
    pair<Node*, destinationNodeLocation> n = navigateTo(key);
    //if node found, then destinationNodeLocation is "here"
    if(n.second == destinationNodeLocation::here){
        //return the node
        return n.first;
    }
    return nullptr;
}


template <typename K, typename V, typename C>
bool bst<K,V,C>::insert(const K& key, const V val) noexcept{
    pair<Node*, destinationNodeLocation> father = navigateTo(key);
    unique_ptr<Node> newNode{new Node(key, val)};
    bool inserted{insertBelow(newNode, father)};
    if (inserted)
        ++_size;
    return inserted;
}

template <typename K, typename V, typename C>
childhoodType bst<K,V,C>::revertChType(childhoodType c) const{
    if(c == childhoodType::leftChild || c == childhoodType::rightChild){
        return (c == childhoodType::leftChild ? childhoodType::rightChild : childhoodType::leftChild);
    }
    throw runtime_error("Reverting childhood type is only available for left and rightChild");
}


template <typename K, typename V, typename C>
bool bst<K,V,C>::erase(const K& key) noexcept{
    
    //search for node
    Node* localHead {getNode(key)};
    //if node doesn't exist, return false
    if(localHead == nullptr){
        return false;
    }

    //If node exists and tree has size 1 -> just call clear.
    if(this->_size ==1){
        this->clear();
        return true;
    }

    //localHeadFather stores info on father of localHead
    Node* localHeadFather(nullptr);

    childhoodType cht_localHead  = childhoodType::root;

    //if node to release is head, just release it
    if(key == head->getKey()){
        localHead = head.release();

    //otherwise, store father info and release local head
    }else{
        localHeadFather = localHead->getFather();
        cht_localHead = localHead->getChildhoodType();
        
        localHead = localHead->release();
    }
    //now that localHead isn't owned anymore inside the tree, I can own it in a new pointer
    unique_ptr<Node> localHead_smart {localHead};

    //search for candidate replacement of localHead left and right of localHead
    Node* candidate =  localHead_smart->biggestOfSmaller();
    if (candidate == nullptr)
        candidate =  localHead_smart->smallestOfBigger();
  
    //release right and left subtrees and store them in unique_ptr
    Node* right = localHead_smart->releaseRightChild() ;
    Node* left = localHead_smart->releaseLeftChild();
    
    unique_ptr<Node> right_smart = unique_ptr<Node> (right);
    unique_ptr<Node> left_smart = unique_ptr<Node> (left);

    

    //prmoted is set to be the new localHead
    Node* promoted = candidate;
    //store the old links of candidate (up, left, right)
    if(candidate->getFather()!=nullptr){
        
        childhoodType cht_candidate = candidate->getChildhoodType();
        Node* candidateFather = candidate->getFather();

        promoted = candidate->release();

        //search for child of candidate on the other side of candidate's cildhoodType
        childhoodType inv_cht_candidate = revertChType(cht_candidate);

        //if it exists, attach it to candidate's father while inverting its childhoodType 
        if(candidate->getChild(inv_cht_candidate) != nullptr){
            Node* candidateChild = promoted->releaseChild(inv_cht_candidate);
            unique_ptr<Node> candidateChildNode {unique_ptr<Node>(candidateChild)};

            candidateFather->setChild(candidateChildNode, cht_candidate);
        }

    }

        

    //delete old localHead and reset it with the promoted node
    localHead_smart.reset(promoted);

    //if left subtree of old localHead existed and the candidate wasn't direct child of old localHead 
    //attach old left subtree to the new localHead
    if(left_smart.get()!=nullptr && candidate!=left_smart.get())
        localHead_smart->setLeftChild(left_smart);
    //same with right
    if(right_smart.get()!=nullptr && candidate!=right_smart.get())
        localHead_smart->setRightChild(right_smart);  
    
    //if old localHead wasn't head of the tree, attach new localHead to old father
    if(cht_localHead != childhoodType::root){
        localHeadFather->setChild(localHead_smart, cht_localHead);
    //otherwise, just set a new head
    }else{
        head.swap(localHead_smart);
    }

    
    this->_size--;

    //release old left and right subtrees
    //(effective only if they weren't reattached, otherwise it's just release on a nullptr)
    left_smart.release();
    right_smart.release();
    return true;
}   


template <typename K, typename V, typename C>
bool bst<K,V,C>::insertBelow(unique_ptr<typename bst<K,V,C>::Node>& nodeToInsert, pair<typename bst<K,V,C>::Node*, destinationNodeLocation> father){
    //evaluate status of candidate father found
    switch(father.second){
        case destinationNodeLocation::head:
            head.swap(nodeToInsert); break;
        case destinationNodeLocation::here:
            //if node already exists, don't insert it but return false;
            return false;
        case destinationNodeLocation::left:
            father.first->setLeftChild(nodeToInsert); break;
        case destinationNodeLocation::right:
            father.first->setRightChild(nodeToInsert); break;
        default:
            throw runtime_error("Unexpected value for destinationNodeLocation");
    }
    return true;
}

template <typename K, typename V, typename C>
bool bst<K,V,C>::insertBelow(unique_ptr<typename bst<K,V,C>::Node>& nodeToInsert, typename bst<K,V,C>::Node* father, childhoodType nodeChType){
    //evaluate childhoodtype and convert it in destinationNodeLocation to call another version of insertBelow
    switch(nodeChType){
        case childhoodType::root:
            return insertBelow(nodeToInsert, make_pair(father, destinationNodeLocation::head)); 
        case childhoodType::leftChild:
            return insertBelow(nodeToInsert, make_pair(father, destinationNodeLocation::left)); 
        case childhoodType::rightChild:
            return insertBelow(nodeToInsert, make_pair(father, destinationNodeLocation::right)); 
        default:
            throw runtime_error("Unexpected value for childhoodType in insertBelow");
    }
}





template <typename K, typename V, typename C>
typename bst<K,V,C>::Iterator bst<K,V,C>::begin() noexcept{
    Node* actual = this->getSmallest();
    return Iterator(actual);
}

template <typename K, typename V, typename C>
typename bst<K,V,C>::ConstIterator bst<K,V,C>::begin() const noexcept{
    Node* actual = this->getSmallest();
    return ConstIterator(actual);
}

template <typename K, typename V, typename C>
typename bst<K,V,C>::Iterator bst<K,V,C>::find(const K& key) noexcept{
    Node* n = getNode(key);
    if(n==nullptr)
        return end();
    return Iterator(n);
}


template <typename K, typename V, typename C>
typename bst<K,V,C>::Node* bst<K,V,C>::getSmallest() const{
    Node* actual = head.get();
    
    if(actual != nullptr){
        while(actual->getLeftChild()!=nullptr){
            actual = actual->getLeftChild();
        }
    }

    return actual;
}

template <typename K, typename V, typename C>
unique_ptr<vector<typename bst<K,V,C>::Node*>> bst<K,V,C>::flatten() const{
    //get all Nodes* in the tree (in hierarchical order) and insert them in array

    //flat array stored in heap to avoid stack overflow
    unique_ptr<vector<Node*>> flat {new vector<Node*>{}};

    //push all Nodes
    Node* node = getSmallest();
    while(node != nullptr){
        flat->push_back(node);
        node = node->next();
    }

    return flat;
}

template <typename K, typename V, typename C>
void bst<K,V,C>::insertMedian(unique_ptr<vector<typename bst<K,V,C>::Node*>>& flat, int beginIndex, int endIndex, typename bst<K,V,C>::Node* newFather,
                                childhoodType medianChildhoodType) {
    // begin greater than end -> exit condition from recursion
    if(beginIndex>endIndex)
        return;
    

    int medianIndex = (beginIndex+endIndex)/2;
    
    Node* medianNode = flat->operator[](medianIndex);
    

    unique_ptr<Node> medianNodeSmart {medianNode};
    //insert node below new father
    bool inserted = insertBelow(medianNodeSmart, newFather, medianChildhoodType);

    if(!inserted)
        throw runtime_error("Error: could not rebalance node " + medianNode->getKey());

    //if end greater than begin, apply recursion to left and right subvector
    if(beginIndex < endIndex){
        insertMedian(flat, beginIndex, medianIndex-1, medianNode, childhoodType::leftChild);
        insertMedian(flat, medianIndex+1, endIndex, medianNode, childhoodType::rightChild);
    }

    
}

template <typename K, typename V, typename C>
void bst<K,V,C>::releaseAllNodes() noexcept{
    head.release()->release_recursive();
}

template <typename K, typename V, typename C>
void bst<K,V,C>::balance() noexcept{
    unique_ptr<vector<Node*>> flat {flatten()};

    releaseAllNodes();

    insertMedian(flat, 0, flat->size() - 1);

}

template <typename K, typename V, typename C>
void bst<K,V,C>::print(bool printStructure) noexcept{
    Node* n = getSmallest();
    while(n != nullptr){
        cout << n->getKey() << ": " << n->getVal()
             << (printStructure ? string() + " (" + static_cast<char>(n->getChildhoodType()) + (")") : "") << "; ";
        n = n->next();
    }
    cout << endl;
}

template <typename K, typename V, typename C>
V& bst<K,V,C>::operator[](const K& k) noexcept{
    pair<Node*, destinationNodeLocation> n = navigateTo(k);
    if(n.second == destinationNodeLocation::here){
        return n.first->getVal();
    }
    //if node doesn't exist, create it and insert it
    unique_ptr<Node> newNode {new Node(k, V{})};
    insertBelow(newNode, n);
    return newNode->getVal();
}

//>>>>>>>>>>>>>>>>>>>>>>>>>METHODS FOR NODE

//copy constructor for Node 
template <typename K, typename V, typename C>
bst<K,V,C>::Node::Node(const bst<K,V,C>::Node& other) : k{other.k}, v{other.v}, c{other.c}{
    
    this->left=nullptr;
    this->right=nullptr;
    this->up=nullptr;
}

template <typename K, typename V, typename C>
pair<typename bst<K,V,C>::Node*, destinationNodeLocation> bst<K,V,C>::Node::navigateTo(const K& key) {
    //if the key to search for is the same as the current node
        //I have found the node, return it + indication that it's here
    if(this->key_equals(key)){
        return make_pair(this, destinationNodeLocation::here);
    }
    //evaluate the key wrt the order relation
    if(this->key_superior(key)){
        //if a left child exists, apply recursion to the left
        if(this->getLeftChild() != nullptr)
            return this->getLeftChild()->navigateTo(key);
        //otherwise, the node doesn't exist and return actual node + indication that the
        //node to be searched for should be left ot the current node
        else
            return make_pair(this, destinationNodeLocation::left);
    }else{
        if(this->getRightChild() != nullptr)
            return this->getRightChild()->navigateTo(key);
        else
            return make_pair(this, destinationNodeLocation::right);
    }
}

template <typename K, typename V, typename C>
typename bst<K,V,C>::Node* bst<K,V,C>::Node::getChild(childhoodType ch) const{
    if(ch==childhoodType::rightChild)
        return right.get();
    if(ch==childhoodType::leftChild)
        return left.get();
    throw runtime_error("Invalid childhood type in method getChild");
}

template <typename K, typename V, typename C>
void bst<K,V,C>::Node::setChild(unique_ptr<Node>& node, childhoodType cht){
    if(cht==childhoodType::leftChild){
        setLeftChild(node); return;
    }
    if(cht==childhoodType::rightChild){
        setRightChild(node); return;
    }
    throw runtime_error("Invalid childhood type for setChild method");
}

template <typename K, typename V, typename C>
typename bst<K,V,C>::Node* bst<K,V,C>::Node::releaseChild(childhoodType ch) {

    if(ch==childhoodType::rightChild){
        return this->releaseRightChild();
    }

    if(ch==childhoodType::leftChild){
        return this->releaseLeftChild();
    }

    throw runtime_error("Invalid childhood type in method releaseChild");
}  

template<typename K, typename V, typename C>
typename bst<K,V,C>::Node* bst<K,V,C>::Node::releaseLeftChild() noexcept {
    if(left.get()==nullptr)
        return nullptr;
    left->setFather(nullptr);
    return this->left.release();
}   


template<typename K, typename V, typename C>
typename bst<K,V,C>::Node* bst<K,V,C>::Node::releaseRightChild() noexcept {
    if(right.get()==nullptr)
        return nullptr;
    right->setFather(nullptr);
    return this->right.release();
}   

template <typename K, typename V, typename C>
typename bst<K,V,C>::Node* bst<K,V,C>::Node::release(){
    if(this->getFather() != nullptr){
        return this->getFather()->releaseChild(this->getChildhoodType());
    }
    throw runtime_error("Can't release head from node itself, use head.release() from bst");
}

template <typename K, typename V, typename C>
typename bst<K,V,C>::Node* bst<K,V,C>::Node::release_recursive(){
    //apply recursion to left and right children
    if(this->getLeftChild() != nullptr){
        this->getLeftChild()->release_recursive();
    }
    if(this->getRightChild() != nullptr){
        this->getRightChild()->release_recursive();
    }
    //if not root, release
    if(this->getChildhoodType() != childhoodType::root){
        return this->release();
        //return this->getFather()->releaseChild(this->getChildhoodType());
    }
    //corner case from root
    return this;

}


template <typename K, typename V, typename C>
typename bst<K,V,C>::Node* bst<K,V,C>::Node::biggestOfSmaller() noexcept{
    //to find biggest node of smallers, just go left once, then right until nullptr
    Node* candidate = this -> getLeftChild();
    if(candidate == nullptr)
        return candidate;
    while(candidate->getRightChild()!=nullptr){
        candidate = candidate -> getRightChild();
    }
    return candidate;
    
}

template <typename K, typename V, typename C>
typename bst<K,V,C>::Node* bst<K,V,C>::Node::smallestOfBigger() noexcept{
    //just go right once, then left untill nullptr
    Node* candidate = this -> getRightChild();
    if(candidate == nullptr)
        return candidate;
    while(candidate->getLeftChild()!=nullptr){
        candidate = candidate -> getLeftChild();
    }
    return candidate;
    
}

template <typename K, typename V, typename C>
typename bst<K,V,C>::Node* bst<K,V,C>::Node::next() noexcept{
    Node* actual  = this;

    Node* next = actual->smallestOfBigger();

    if(next!=nullptr){
        actual = next;  
    }else{
        evalChildhoodType(&actual);
    }
    
    return actual;
}

template <typename K, typename V, typename C>
void bst<K,V,C>::Node::evalChildhoodType(Node** actual) {
    //evaluate childhood type to get next node when it can't be found
    //within the subtree of the node itself
    switch((*actual)->getChildhoodType()){
        case (childhoodType::leftChild):
            *actual = (*actual)->getFather(); break;
        case(childhoodType::rightChild):
            //if rightchild, the node is found by recursively climbing the tree until
            //we don't find another left child.
            *actual = (*actual)->getFather();
            evalChildhoodType(actual); break;
        case(childhoodType::root):
            //if my climb reaches the head, it means no next node is found
            //and the traversing is over
            *actual = nullptr; break;
        default:
            throw runtime_error("Unexpected node childhoodType in next node search");
    }
}




#endif