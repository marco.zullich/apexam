#include "bst.h"


int main(){
    cout << "start bst testing" << endl;
    
    bst<int,int> b;
    
    b.insert(10,6);
    b.insert(7,5);
    b.insert(6,4);
    b.insert(5,3);
    b.insert(4,2);
    b.insert(3,1);

    cout << "tree b: ";
    b.print(true);

    //copy-constructor
    bst<int,int> c {b};

    c.erase(10);

    cout << "tree c: ";
    c.print(true);

    //move ctor
    bst<int,int> d {move(b)};
    cout << "tree b (after move): ";
    b.print(true);
    cout << "tree d: ";
    d.print(true);

    //copy assmnt
    b = d;
    b.balance();
    cout << "tree b (after assmnt and balancing): ";
    b.print(true);

    //non const operator[]
    c[7] = 111;
    cout << "tree c (after []): ";
    c.print(true);
    //c.print();

    //traversing with iterator
    cout << "d traversal: ";
    bst<int,int>::ConstIterator it = d.cbegin();
    while(it!=d.cend()){
        cout << *it << "; ";
        ++it;
    }
    cout << endl;
    
    //move assmnt
    c  = move(b);
    cout << "tree b (after move): ";
    b.print(true);
    cout << "tree c (after move assmt): ";
    c.print(true);

    cout << " ############# " << endl << "trying out a different ordering" << endl;

    bst<int,string,std::greater<double>> n;
    n.insert(2, "a");
    n.insert(1, "b");
    n.insert(3, "c");
    n.insert(7, "d");
    n.insert(6, "e");
    n.insert(4, "f");
    n.insert(0, "g");

    n.print(true);
    n.balance();
    cout << "n rebalanced" << "\n";
    n.print(true);

    cout << " ############# " << endl << "custom ordering\n";
    struct StrLenComparator : public std::binary_function<string, string, bool>{
        bool operator()(string s1, string s2){return s1.size()<s2.size();}
    };
    bst<string, int, StrLenComparator> l;
    l.insert("i", 2);
    l.insert("", 1);
    l.insert("aa",3);
    l.insert("aaa",4);
    l.insert("aa",0); //no insert
    l.print();
    
}
