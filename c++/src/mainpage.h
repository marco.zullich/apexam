/*! \mainpage Binary Search Tree
 *
 * \section intro_sec Introduction
 *
 * Hereby we illustrate how to compile, run and use this Binary Search Tree C++ implementation
 *
 * \section org_sec Code Organization
 * 
 * The code for the implementation is wholly contained within the file `bst.h` in the `include` folder.
 * A `bst_py.cpp` source file is stored within the `src` folder which contains the `pybind11` code to expose the `bst` class
 * within a Python shared library.
 *
 * \section comp_sec Compilation
 * 
 * \subsection comp_cpp_sub C++
 * 
 * After having created a source file which includes `bst.h`, run the makefile within the C++ folder setting the variable
 * `SRC` to the file's path.
 * Alternatively, the example file `bst.cpp` from the `src` folder may be used.
 * 
 * \subsection comp_py_sub Python shared library
 * 
 * Prerequisites for the library compilation are:
 * - `python3-dev` package (install via `sudo apt.get`)
 * - `pybind11` Python package (install via `pip3 install`)
 *
 * run the makefile within the `mix` folder
 * 
 * \section use_sec C++ usage
 * 
 * Please refer to the file `src/bst.cpp` for an example of usage of the `bst`.
 * 
 */