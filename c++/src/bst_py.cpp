//#####PYTHON EXPOSURE#####
#include "../include/bst.h"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/iostream.h>

namespace py = pybind11;



PYBIND11_MODULE(bst, m){
    py::class_<bst<string,int>>(m, "bst")
        .def(py::init<>())
        .def(py::init<const bst<string,int>&>())
        .def("insert", &bst<string,int>::insert)
        .def("print", &bst<string,int>::print,
                py::call_guard<py::scoped_ostream_redirect,
                                py::scoped_estream_redirect>(),
                py::arg("printStructure") = false)                   
        .def("find", [](bst<string,int>& b, string& k){
            return py::make_iterator(b.find(k), b.end());
        }, py::keep_alive<0, 1>())
        .def("__iter__", [](bst<string,int>& b){
            return py::make_iterator(b.begin(), b.end());
        }, py::keep_alive<0, 1>())
        .def("__getitem__", [](bst<string,int>& b, string& k){
            return b[k];
        })
        .def("clear",&bst<string,int>::clear)
        .def("erase",&bst<string,int>::erase)
        .def("balance",&bst<string,int>::balance);
        

        //.def("get_head",&bst<string,int>::getHead);

    //py::enum_<bst<string,int>::childhoodType>(m, "childhoodType")
    //    .value("root", bst<string,int>::childhoodType::root)
    //    .value("leftChild", bst<string,int>::childhoodType::leftChild)
    //    .value("rightChild", bst<string,int>::childhoodType::rightChild)
    //    .export_values();

    py::class_<bst<string,int>::Iterator>(m, "bst_iterator");


}