#include "../include/bst.h"
#include <chrono>
#include <fstream>
#include <algorithm>    // std::sort

int main(){
    ofstream outFile("timings_small.txt");
    
    cout << "start bst testing" << endl;
    bst<int,int> unbalanced;
    bst<int,int> balanced;
    
    int count=1000;
    int skip=100;
    double durationUnbalanced;
    double durationBalanced;
    //cout << "st"<<inner << endl;
    std::vector<double> timings;
    for(int i = 0; i < count; i++)
    {
        unbalanced.insert(i,i);
        balanced.insert(i,i);

       
        if(i%skip==0){
            //unbalanced
             cout<<"valore: "<<*unbalanced.find(i);                
            auto start = chrono::steady_clock::now();
            unbalanced.find(i);
            auto end = chrono::steady_clock::now();
            auto diff = end - start;
            durationUnbalanced = chrono::duration <double, milli> (diff).count();
            cout << "unbalanced: "<<i<<" time: "<<durationUnbalanced << " ms" << endl;
            
            
            //balanced
            balanced.balance();
            cout<<"valore: "<<*balanced.find(i);                
            start = chrono::steady_clock::now();
            balanced.find(i);
            end = chrono::steady_clock::now();
            diff = end - start;
            durationBalanced = chrono::duration <double, milli> (diff).count();
            cout << "balanced: "<<i<<" time: "<<durationBalanced << " ms" << endl;
            outFile << i<<", "<<durationUnbalanced << ", "<<durationBalanced<<endl;
        }
    }
}
