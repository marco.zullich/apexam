# Mix

## Report

This report illustrates how to compile and import in Python3 the data structure `bst` created within the C++ part of the project.

### Prerequisites

In orded to compile the C++ source into a Python shared library, Pybind11 has to be installed.
A prerequisite of Pybind11 is `python3-dev`.
pybind can be installed via via pip:
`pip3 install pybind11`
The python3-dev package can be installed using apt-get with the following command:
`sudo apt-get install python3-dev`

### How to compile

To compile, just use the makefile found within this folder.

To compile manually, type into the terminal:
``g++ -std=c++11 -fPIC -shared -I`python3 -m pybind11 --includes` ../c++/src/bst_py.cpp -o bst`python3-config --extension-suffix` ``
This will create, within this folder, a `.so` file ready to import into Python3.

### How to import

To import the library within Python3, just run the following command within a Python3 shell: `from bst import *`; then a new `bst` object may be called running `b = bst()`. The public methods of `bst` are the same of the C++ implementations.

Within the folder there's a `bst_tests.py` file which stores tests for the `bst` object, used during the implementation mainly to avoid regressions while adding new features. The file may be treated as an example of usage of `bst`.

### Implementation report
The code for Python exposure is very short and may be found within the `c++/src/python3_bst.cpp` file.

There, in order of appearence, are defined the following methods:
- Default constructor;
- Copy constructor;
- `insert` method;
- `print` method, with additional code to:
    - convey the standard output stream of C++ within the stdout of Python
    - tell Python that the argument of the method defaults to `false`
- `find` method, with additional code to transform the output from a C++-style iterator to a Python iterator;
- `__iter__` method, which returns a Python-style iterator in place of the C++-style one;
- `__getitem__` method, or `[]` operator;
- `clear` method;
- `erase` method;
- `balance` method;

Note that the methods work in the same exact way of the C++ implementation, besides the `[]` operator: due to an unsolved bug noticed only within the Python version of `bst`, while specifying a nonexistent key within the brackets, the program crashes yielding a segfault. This behaviour was not present in the C++ implementation and may be due to errors in Pybind11.

## Python interface

Write a Python interface to the binary tree you implemented in c++, such that you can construct, traverse and print the tree from a python program.

You are free to adopt the strategy and design you prefer for the interface.

### What you have to submit

- You have to upload only and all the **source files** you wrote (eiter c++ or python), with a
**Makefile** (or **CMakeLists.txt**) and a **readme.md** file where you describe how to compile, run your code and a short report on what you have done and understood.

