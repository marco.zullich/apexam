import bst
import unittest
from io import StringIO
from contextlib import redirect_stdout
import sys

class Test(unittest.TestCase):

    @classmethod
    def setUp(self):
        self._tree = bst.bst()
        #self.node = bst.Node("a",1)

    def test_treeTraversalWithSingleNode(self):
        self._tree.insert("a",1)
        it = iter(self._tree)
        self.assertEqual(next(it), 1)

    def test_TreeTraversalWithThreeNodes(self):
        self._tree.insert("a",1)
        self._tree.insert("aa",2)
        self._tree.insert("b",3)
        self._tree.insert("c",4)

        it = iter(self._tree)
        i = 1
        while(True):
            try:
                self.assertEqual(next(it),i)
                i+=1
            except StopIteration:
                break

    def test_LongTreeTraversal(self):
        self._tree.insert("d",8)
        self._tree.insert("c",6)
        self._tree.insert("b",2)
        self._tree.insert("a",1)
        self._tree.insert("bc",4)
        self._tree.insert("ba",3)
        self._tree.insert("be",5)
        self._tree.insert("ca",7)
        self._tree.insert("daa",10)
        self._tree.insert("da",9)
        self._tree.insert("df",17)
        self._tree.insert("de",16)
        self._tree.insert("dd",13)
        self._tree.insert("dc",12)
        self._tree.insert("db",11)
        self._tree.insert("ddd",15)
        self._tree.insert("ddc",14)
        self._tree.insert("e",18)

        it = iter(self._tree)
        i = 1
        while(True):
            try:
                self.assertEqual(next(it),i)
                i+=1
            except StopIteration:
                break
        

    def test_printShortTree(self):
        self._tree.insert("b",2)
        self._tree.insert("a",1)
        self._tree.insert("ab",3)
        self._tree.insert("c",4)
        self._tree.insert("ba",5)
        self._tree.insert("d",6)

        f = StringIO()
        with redirect_stdout(f):
            self._tree.print()   
            #print("JJ")
        printOutput = f.getvalue()
    
        
        self.assertEqual(printOutput, "a: 1; ab: 3; b: 2; ba: 5; c: 4; d: 6; \n")

    def test_find(self):
        self._tree.insert("b",2)
        self._tree.insert("a",1)
        self._tree.insert("ab",3)
        self._tree.insert("c",4)
        self._tree.insert("ba",5)
        self._tree.insert("d",6)

        self.assertEqual(next(self._tree.find("c")), 4)
        self.assertEqual(next(self._tree.find("d")), 6)

    def test_findNotFound(self):
        self._tree.insert("b",2)
        self._tree.insert("a",1)
        self._tree.insert("ab",3)
        self._tree.insert("c",4)
        self._tree.insert("ba",5)
        self._tree.insert("d",6)

        it = self._tree.find("h")


        self.assertRaises(StopIteration, next, it)

    def test_squareBracketsExistingValues(self):
        self._tree.insert("b",2)
        self._tree.insert("a",1)
        self._tree.insert("ab",3)
        self._tree.insert("c",4)
        self._tree.insert("ba",5)
        self._tree.insert("d",6)

        self.assertEqual(self._tree["b"], 2)
        self.assertEqual(self._tree["ba"], 5)
    

    def test_copy_constructor(self):
        self._tree.insert("b",2)
        self._tree.insert("a",1)
        self._tree.insert("ab",3)
        self._tree.insert("c",4)
        self._tree.insert("ba",5)
        self._tree.insert("d",6)

        copied = bst.bst(self._tree)

        f = StringIO()
        with redirect_stdout(f):
            copied.print()   
            #print("JJ")
        printOutput = f.getvalue()
    
        
        self.assertEqual(printOutput, "a: 1; ab: 3; b: 2; ba: 5; c: 4; d: 6; \n")
    
    def test_clear(self):
        self._tree.insert("b",2)
        self._tree.insert("a",1)
        self._tree.insert("ab",3)
        self._tree.insert("c",4)
        self._tree.insert("ba",5)
        self._tree.insert("d",6)

        self._tree.clear()

        f = StringIO()
        with redirect_stdout(f):
            self._tree.print()   
            printOutput = f.getvalue()
    
        
        self.assertEqual(printOutput, "\n")

    def test_erase_head_small(self):
        self._tree.insert("b",2)
        self._tree.insert("a",1)
        self._tree.insert("c",3)
        
        self._tree.erase("b")

        f = StringIO()
        with redirect_stdout(f):
            self._tree.print()   
            printOutput = f.getvalue()
    
        
        self.assertEqual(printOutput, "a: 1; c: 3; \n")

    def test_erase_head_a_bit_bigger(self):
        self._tree.insert("b",2)
        self._tree.insert("a",1)
        self._tree.insert("c",3)
        self._tree.insert("ab",4)
        
        self._tree.erase("b")

        f = StringIO()
        with redirect_stdout(f):
            self._tree.print()   
            printOutput = f.getvalue()
    
        
        self.assertEqual(printOutput, "a: 1; ab: 4; c: 3; \n")

    def test_erase_head_only_right(self):
        self._tree.insert("a",1)
        self._tree.insert("b",2)
        
        
        
        self._tree.erase("a")

        f = StringIO()
        with redirect_stdout(f):
            self._tree.print()   
            printOutput = f.getvalue()
    
        
        self.assertEqual(printOutput, "b: 2; \n")

    def test_erase_head_only_left(self):
        self._tree.insert("b",1)
        self._tree.insert("a",2)
        
        
        
        self._tree.erase("b")

        f = StringIO()
        with redirect_stdout(f):
            self._tree.print()   
            printOutput = f.getvalue()
    
        
        self.assertEqual(printOutput, "a: 2; \n")
            
    def test_erase_head_left_candidate_with_children(self):
        self._tree.insert("b",2)
        self._tree.insert("a",1)
        self._tree.insert("c",3)
        self._tree.insert("ab",4)
        self._tree.insert("aa",5)

        self._tree.erase("b")

        f = StringIO()
        with redirect_stdout(f):
            self._tree.print()   
            printOutput = f.getvalue()
    
        
        self.assertEqual(printOutput, "a: 1; aa: 5; ab: 4; c: 3; \n")

    def test_erase_head_right_candidate_with_children(self):
        self._tree.insert("b",2)
        self._tree.insert("d",3)
        self._tree.insert("c",4)
        self._tree.insert("ca",5)

        self._tree.erase("b")

        f = StringIO()
        with redirect_stdout(f):
            self._tree.print()   
            printOutput = f.getvalue()
    
        
        self.assertEqual(printOutput, "c: 4; ca: 5; d: 3; \n")

    def test_erase_only_head(self):
        self._tree.insert("b",2)
        

        self._tree.erase("b")

        f = StringIO()
        with redirect_stdout(f):
            self._tree.print()   
            printOutput = f.getvalue()
    
        
        self.assertEqual(printOutput, "\n")

    def test_printByLevel(self):
        self._tree.insert("d",8)
        self._tree.insert("c",6)
        self._tree.insert("b",2)
        self._tree.insert("a",1)
        self._tree.insert("bc",4)
        self._tree.insert("ba",3)
        self._tree.insert("be",5)
        self._tree.insert("ca",7)
        self._tree.insert("daa",10)
        self._tree.insert("da",9)
        self._tree.insert("df",17)
        self._tree.insert("de",16)
        self._tree.insert("dd",13)
        self._tree.insert("dc",12)
        self._tree.insert("db",11)
        self._tree.insert("ddd",15)
        self._tree.insert("ddc",14)
        self._tree.insert("e",18)

        f = StringIO()
        with redirect_stdout(f):
            self._tree.print(True)   
            printOutput = f.getvalue()

        expectedOutput = "a: 1 (L); b: 2 (L); ba: 3 (L); bc: 4 (R); be: 5 (R); " +\
                         "c: 6 (L); ca: 7 (R); d: 8 (H); da: 9 (L); daa: 10 (R); " +\
                         "db: 11 (L); dc: 12 (L); dd: 13 (L); ddc: 14 (L); ddd: 15 (R); " +\
                         "de: 16 (L); df: 17 (R); e: 18 (R); \n"
        self.assertEqual(printOutput, expectedOutput)

    def test_balanceSmallLeft(self):
        self._tree.insert("d",8)
        self._tree.insert("c",6)
        self._tree.insert("b",2)
        self._tree.insert("a",1)

        self._tree.balance()

        f = StringIO()
        with redirect_stdout(f):
            self._tree.print(True)   
            printOutput = f.getvalue()

        expectedOutput = "a: 1 (L); b: 2 (H); c: 6 (R); d: 8 (R); \n"
        self.assertEqual(printOutput, expectedOutput)

        def test_balanceSmallRight(self):
            self._tree.insert("a",1)
            self._tree.insert("b",2)
            self._tree.insert("c",3)
            self._tree.insert("d",4)

            self._tree.balance()

            f = StringIO()
            with redirect_stdout(f):
                self._tree.print(True)   
                printOutput = f.getvalue()

            expectedOutput = "a: 1 (L); b: 2 (H); c: 3 (R); d: 4 (R); \n"
            self.assertEqual(printOutput, expectedOutput)

        def test_balanceLarge(self):
            self._tree.insert("d",8)
            self._tree.insert("c",6)
            self._tree.insert("b",2)
            self._tree.insert("a",1)
            self._tree.insert("bc",4)
            self._tree.insert("ba",3)
            self._tree.insert("be",5)
            self._tree.insert("ca",7)
            self._tree.insert("daa",10)
            self._tree.insert("da",9)
            self._tree.insert("dd",13)
            self._tree.insert("dc",12)
            self._tree.insert("db",11)
            self._tree.insert("ddc",15)
            self._tree.insert("ddb",14)

            self._tree.balance()

            f = StringIO()
            with redirect_stdout(f):
                self._tree.print(True)   
                printOutput = f.getvalue()

            expectedOutput = "a: 1 (L); b: 2 (L); ba: 3 (R); bc: 4 (L); be: 5 (L); " +\
                         "c: 6 (R); ca: 7 (R); d: 8 (H); da: 9 (L); daa: 10 (L); " +\
                         "db: 11 (R); dc: 12 (R); dd: 13 (L); ddb: 14 (R); ddc: 15 (R); \n"
            self.assertEqual(printOutput, expectedOutput)

    '''def test_squareBracketsNotExistingValues(self):
        self._tree.insert("b",2)
        self._tree.insert("a",1)
        self._tree.insert("ab",3)
        self._tree.insert("c",4)
        self._tree.insert("ba",5)
        self._tree.insert("d",6)

        self._tree["e"]

        self.assertEqual(next(self._tree.find("e")),0)'''

    

if __name__ == '__main__':
    unittest.main()
